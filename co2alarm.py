#!/usr/bin/env python3
import os

from lib.thingspeak import get_latest_co2_levels
from lib.config import load_config

CONFIG = load_config()

CHANNEL_ID = CONFIG['thingspeak_channel_id']
LOW_CO2_LEVEL = CONFIG['alert_levels']['low']
HIGH_CO2_LEVEL = CONFIG['alert_levels']['high']
HOME_FOLDER = CONFIG['home_folder']


def notify(header, message):
    from lib.gui import show_notification
    show_notification(header, message)


def get_current_co2():
    return get_latest_co2_levels(CHANNEL_ID)


def set_environment_variables():
    os.environ["DISPLAY"] = ":0.0"
    os.environ["XAUTHORITY"] = HOME_FOLDER + "/.Xauthority"


def show_high_level_warning_if_needed(co2_levels):
    level1 = co2_levels[0]
    level2 = co2_levels[1]
    is_over = level2 >= HIGH_CO2_LEVEL
    was_under = level1 < HIGH_CO2_LEVEL
    if is_over and was_under:
        show_low_level_notification(level2)


def show_high_level_notification(current_co2):
    message = "Current CO2 level is " + \
        str(current_co2) + ". Open the window."
    title = "CO2 level is too high"
    notify(title, message)


def show_low_level_warning_if_needed(co2_levels):
    level1 = co2_levels[0]
    level2 = co2_levels[1]
    is_under = level2 <= LOW_CO2_LEVEL
    was_over = level1 > LOW_CO2_LEVEL
    if is_under and was_over:
        show_low_level_notification(level2)


def show_low_level_notification(current_co2):
    message = "Current CO2 level is " + \
        str(current_co2) + ". Close the window."
    title = "CO2 level is back to normal"
    notify(title, message)


if __name__ == "__main__":
    set_environment_variables()
    co2_levels = get_current_co2()
    print("Current CO2 is: " + str(co2_levels[1]))
    show_low_level_warning_if_needed(co2_levels)
    show_high_level_warning_if_needed(co2_levels)
