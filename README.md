#  co2-alarm
Co2 alarm for our thingspeak channel using python tk


##  Requirements
Install these packages via pip3:
- pillow
- thingspeak
- pyyaml
```
sudo pip3 install pillow thingspeak pyyaml
```

Install these packages via apt-get:
 - python3-pil.imagetk
 ```
 sudo apt-get install python3-pil.imagetk
 ```


 ## Setup
 Add a cronjob to start the program periodically
 ```
 crontab -e
 ```
 Add this line to your cron file. It will check the CO2 level in every 10 minutes between 8 am to 5 pm in every day between monday and friday.
 ```
 */10 8-17 * * 1-5 export {path-to-repo}/co2alarm.py {path-to-config}
 ```


 ## Configuration
 Not implemented yet.
