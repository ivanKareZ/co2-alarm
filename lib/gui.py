from PIL import Image, ImageTk
import tkinter as tk


def show_notification(header, message):
    root = tk.Tk()
    root.title(header)
    root.attributes("-topmost", True)
    root.lift()
    topframe = tk.Frame(root)
    img = Image.open("image/co2.png")
    img = img.resize((50, 50), Image.ANTIALIAS)
    render = ImageTk.PhotoImage(img)
    tk.Label(topframe, image=render).pack(side=tk.LEFT)
    tk.Label(topframe, text=message).pack(padx=(20, 0), side=tk.LEFT)
    topframe.pack(padx=20, pady=(20, 10))
    tk.Button(root, text='Silence for today', command=root.destroy).pack(
        padx=20, pady=(10, 20), side=tk.LEFT)
    tk.Button(root, text='OK', command=root.destroy).pack(
        padx=20, pady=(10, 20), side=tk.RIGHT)
    root.mainloop()
