import json
import thingspeak


def get_latest_co2_levels(channel_id):
    channel = thingspeak.Channel(channel_id)
    data = channel.get({'results': 2})
    parsed = json.loads(data)
    feeds = parsed['feeds']
    data1 = _get_data_from_feeds(0, feeds)
    data2 = _get_data_from_feeds(1, feeds)
    return [data1, data2]


def _get_data_from_feeds(index, feeds):
    measurement = feeds[index]
    string_data = measurement['field1']
    return int(string_data)
