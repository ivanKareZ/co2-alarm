import yaml
import sys


def load_config():
    if len(sys.argv) < 2:
        print("Please provide a config file.")
        sys.exit(1)
    config_path = sys.argv[1]
    return _load_config_from_path(config_path)


def _load_config_from_path(config_path):
    with open(config_path) as file:
        return yaml.full_load(file)
